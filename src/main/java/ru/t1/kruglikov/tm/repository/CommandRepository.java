package ru.t1.kruglikov.tm.repository;

import ru.t1.kruglikov.tm.constant.ArgumentConst;
import ru.t1.kruglikov.tm.constant.CommandConst;
import ru.t1.kruglikov.tm.model.Command;

public class CommandRepository {

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show help.");
    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version.");
    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show about developer.");
    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system information.");
    public static final Command ARGUMENT = new Command(CommandConst.ARGUMENT, ArgumentConst.ARGUMENT, "Show argument list.");
    public static final Command COMMAND = new Command(CommandConst.COMMAND, ArgumentConst.COMMAND, "Show command list.");
    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    public static final Command[] TERMINAL_COMMANDS = {HELP, VERSION, ABOUT, INFO, ARGUMENT, COMMAND, EXIT};

    public static Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
